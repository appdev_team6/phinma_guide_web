// show/hide nav menu 
const menu = document.querySelector(".nav-menu");
const menuBtn = document.querySelector("#open-menu-btn");
const closeBtn = document.querySelector("#close-menu-btn");

menuBtn.addEventListener('click', () => {
    menu.style.display = "flex";
    closeBtn.style.display = "inline-block";
    menuBtn.style.display = "none";
})

//close nav menu 
const closeNav = () => {
    menu.style.display = "none";
    closeBtn.style.display = "none";
    menuBtn.style.display = "inline-block";
}

closeBtn.addEventListener('click', closeNav)


//tabs

const tabs = document.querySelectorAll(".tab_btn");
const all_content = document.querySelectorAll(".content");

tabs.forEach((tab, index)=>{
    tab.addEventListener('click', (e)=>{
        tabs.forEach(tab=>{tab.classList.remove('active')});
        tab.classList.add('active');

        var line = document.querySelector(".line");
        line.style.width = e.target.offsetWidth + "px";
        line.style.left = e.target.offsetLeft + "px";

        all_content.forEach(content=>{content.classList.remove('active')});
        all_content[index].classList.add('active');
    })
})



const accordionContent = document.querySelectorAll(".accordion-content");

accordionContent.forEach((item, index) => {
    let header = item.querySelector("header");
    let description = item.querySelector(".description");
    let descriptionHeight = description.scrollHeight;

    header.addEventListener("click", () => {
        item.classList.toggle("open");

        if (item.classList.contains("open")) {
            description.style.maxHeight = `${descriptionHeight}px`;
            item.querySelector("i").classList.replace("uil-plus", "uil-minus");
        } else {
            description.style.maxHeight = "0";
            item.querySelector("i").classList.replace("uil-minus", "uil-plus");
        }

        // Close other accordion items
        accordionContent.forEach((otherItem, index2) => {
            if (index !== index2) {
                otherItem.classList.remove("open");
                let otherDescription = otherItem.querySelector(".description");
                otherDescription.style.maxHeight = "0";
                otherItem.querySelector("i").classList.replace("uil-minus", "uil-plus");
            }
        });
    });
});




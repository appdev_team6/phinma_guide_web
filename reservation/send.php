<?php
include 'config.php';

// Collect user reservations from your source (database, form, etc.)

// Set the file path for your CSV file
$csvFilePath = 'user_reservations.csv';

// Open or create the CSV file for writing (append mode)
$csvFile = fopen($csvFilePath, 'a');

if ($csvFile) {
    // Write a header row if the file is empty (only for the first time)
    if (filesize($csvFilePath) == 0) {
        fputcsv($csvFile, array('Reservation Number', 'Student Name', 'Student ID', 'Phone Number', 'Date of Submission', 'Requirements'));
    }

    // Replace these variables with actual reservation data
    $reservationNumber = 'RES-12345';
    $studentName = 'John Doe';
    $studentID = '123456';
    $phoneNumber = '555-123-4567';
    $dateOfSubmission = '2023-10-15';
    $requirements = 'Some requirements here';

    // Write the reservation data to the CSV file
    fputcsv($csvFile, array($reservationNumber, $studentName, $studentID, $phoneNumber, $dateOfSubmission, $requirements));

    // Close the CSV file
    fclose($csvFile);

    echo 'Reservation data saved in CSV file.';
} else {
    echo 'Error: Unable to open CSV file for writing.';
}

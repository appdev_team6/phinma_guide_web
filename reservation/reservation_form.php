<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    <title>School Reservation Form</title>
</head>

<body>
    <div class="reservation-form" data-aos="fade" data-aos-duration="1000">
        <form action="process_reservation.php" method="POST">
            <h2>Reservation Form</h2>
            <div class="flex">
                <div class="inputBox">
                    <label for="student_name">Student Name:</label>
                    <input type="text" name="student_name" class="box" required>

                    <label for="student_id">Student ID:</label>
                    <input type="text" name="student_id" class="box" required>

                    <label for="phone_number">Phone Number:</label>
                    <input type="text" name="phone_number" class="box" required>
                </div>

                <div class="inputBox">
                    <label for="date_of_submission">Date of Submission:</label>
                    <input type="date" name="date_of_submission" class="box" required>

                    <label for="requirements">Specify the requirements to be passed</label>
                    <input type="text" name="requirements" class="box" required>
                </div>
            </div>
            <input type="submit" value="Submit Reservation" name="reservation" class="btn">
            <?php
            echo '<a href="http://127.0.0.1:5501/home.html" class="btn">Go Back</a>';
            ?>
        </form>
    </div>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>
</body>

</html>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    <title>Reservation Confirmation</title>
</head>

<body>
    <div class="confirmation" data-aos="slide-right" data-aos-duration="1000">
        <div class="flex">
            <div class="message-box">
                <h2>Reservation Confirmation</h2>
                <?php
                include 'config.php'; // Include your database configuration file

                if (isset($_GET["reservation_number"])) {
                    $reservation_number = $_GET["reservation_number"];

                    // Prepare and execute a SQL query to fetch reservation details
                    $sql = "SELECT student_name, date_of_submission, requirements FROM user_reservation WHERE reservation_number = ?";
                    $stmt = $conn->prepare($sql);

                    if ($stmt) {
                        $stmt->bind_param("s", $reservation_number);
                        $stmt->execute();
                        $result = $stmt->get_result();

                        // Check if a matching reservation is found
                        if ($result->num_rows > 0) {
                            $row = $result->fetch_assoc();
                            $student_name = $row["student_name"];
                            $date_of_submission = $row["date_of_submission"];
                            $requirements = $row["requirements"];

                            // Display reservation details
                            echo "<p>Your reservation with reservation number <br><b>$reservation_number</b><br> has been confirmed.</p>";
                            echo "<p>Name: <b>$student_name</b></p>";
                            echo "<p>Date: <b>$date_of_submission</b></p>";
                            echo "<p>Requirements to be passed: <b>$requirements</b></p>";
                            echo "<p><b>Note:</b> To safeguard your reservation, it is important that you submit your requirements punctually,  failure to adhere to the deadline will result in the registrar forfeiting your reservation.</p>";
                            // Add more details as needed

                            echo '<a href="http://127.0.0.1:5501/home.html" class="btn">Go Back</a>';
                        } else {
                            echo "Reservation not found.";
                        }

                        // Close the statement
                        $stmt->close();
                    } else {
                        echo "Error in preparing SQL statement: " . $conn->error;
                    }

                    // Close the database connection
                    $conn->close(); // Close the database connection here
                } else {
                    echo "Reservation number not provided.";
                }
                ?>
            </div>
        </div>
    </div>

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>
</body>

</html>
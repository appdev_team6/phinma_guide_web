<?php
// Include your database configuration file
include 'config.php';

// Check if the form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Retrieve form data
    $student_name = $_POST["student_name"];
    $student_id = $_POST["student_id"];
    $phone_number = $_POST["phone_number"];
    $date_of_submission = $_POST['date_of_submission'];
    $requirements = $_POST["requirements"];

    // Generate a reservation number
    $reservation_number = "RES-" . uniqid();

    // Insert reservation data into the database
    $query = "INSERT INTO user_reservation (student_name, student_id, phone_number, date_of_submission, requirements, reservation_number)
              VALUES ('$student_name', '$student_id', '$phone_number', '$date_of_submission', '$requirements', '$reservation_number')";

    // Check if the query was successful
    if (mysqli_query($conn, $query)) {
        // Send confirmation email (implement this)
        $subject = "Reservation Confirmation";
        $message = "Dear $student_name,\n\nYour reservation with reservation number $reservation_number has been confirmed.\n\nThank you for using our reservation system.";
        mail($email, $subject, $message);

        // Save reservation data to a CSV file
        $csvFile = 'user_reservations.csv';
        $csvData = array($reservation_number, $student_name, $student_id, $phone_number, $date_of_submission, $requirements);
        $handle = fopen($csvFile, 'a');

        // Check if the CSV file was opened successfully
        if ($handle) {
            fputcsv($handle, $csvData);
            fclose($handle);
        } else {
            echo "Error: Unable to open CSV file for writing.";
        }

        // Redirect to a confirmation page
        header("Location: confirmation.php?reservation_number=$reservation_number");
        exit();
    } else {
        echo "Error: " . mysqli_error($conn);
    }
}

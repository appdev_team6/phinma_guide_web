<?php
include 'config.php';

// Start the session at the beginning of your PHP code
session_start();

$messageEmailPass = '';
$messageUserNotExist = '';

if (isset($_POST['submit'])) {
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $pass = mysqli_real_escape_string($conn, md5($_POST['password']));

    $select = mysqli_query($conn, "SELECT * FROM `user_form` WHERE email = '$email' AND password = '$pass'") or die('Query failed');

    if (mysqli_num_rows($select) > 0) {
        $row = mysqli_fetch_assoc($select);
        $_SESSION['user_id'] = $row['id'];
        header('location:http://127.0.0.1:5501/home.html');
    } else {
        $userExistsQuery = mysqli_query($conn, "SELECT * FROM `user_form` WHERE email = '$email'") or die('User check query failed');

        if (mysqli_num_rows($userExistsQuery) > 0) {
            $messageEmailPass = 'Incorrect email or password!';
        } else {
            $messageUserNotExist = 'User does not exist.';
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>

    <!-- custom css file link  -->
    <link rel="stylesheet" href="css_files/style.css">
</head>
<body>

<div class="form-container">
    <form action="" method="post" enctype="multipart/form-data">
        <h3>Login now</h3>
        <?php
        if (!empty($messageEmailPass)) {
            echo '<div class="message">' . $messageEmailPass . '</div>';
        }
        if (!empty($messageUserNotExist)) {
            echo '<div class="message">' . $messageUserNotExist . '</div>';
        }
        ?>
        <input type="email" name="email" placeholder="Email" class="box" required>
        <input type="password" name="password" placeholder="Password" class="box" required>
        <input type="submit" name="submit" value="login now" class="btn">
        <p>Don't have an account? <a href="register.php">Register now</a></p>
    </form>
</div>
</body>
</html>
